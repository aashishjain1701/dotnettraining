﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace DotNetTraining
{
    class TypeConversions
    {
        static void Main(string[] args)
        {
            int myNum = 10;
            double myFloat = 111.45;
            char myChar = '1';
            string mySentence = "abc";
            bool myBool = true;
            Console.WriteLine("Explicit Type Conversion\n");
            Console.WriteLine("float to string: " + Convert.ToString(myNum));    
            Console.WriteLine("int to float: " + Convert.ToDouble(myNum));    
            Console.WriteLine("float to int: " + Convert.ToInt32(myFloat));  
            Console.WriteLine("boolean to string: " + Convert.ToString(myBool));
            Console.WriteLine("\n");

            Console.WriteLine("Implicit Type Conversion\n");
            double myDouble = myNum;
            double myDoubleNum = myChar;
            int myInteger = myChar;
            Console.WriteLine( "int to float: " + myDouble);
            Console.WriteLine("char to int: " + myInteger);
            Console.WriteLine("char to float: " + myDoubleNum);
        }
    }
}